# Build cache

```bash
docker build -t use-cache . 
```

Add `COPY ./tmp_4_file.something .`
And build again
```bash
docker build -t use-cache . 
```

Add a line to `tmp_2_file.something`:
And build again
```bash
echo "hello\n" >> tmp_2_file.something
docker build -t use-cache . 
```


