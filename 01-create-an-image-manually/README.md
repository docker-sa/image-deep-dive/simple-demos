# Create an image manually

```bash
docker run --name demo --rm -it ubuntu
node --version # error
apt update && apt install -y nodejs
node --version

# in another terminal
docker commit demo custom-node

docker run --rm -it custom-node
node --version
```

✋ look at the image panel of the Docker Desktop GUI
